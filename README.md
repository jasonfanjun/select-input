selectInput2.0版本已出，如果需要可以移步到到 [select-input2.x](https://gitee.com/JerryZst/select-input2.x)，全新的开发模式，更全面的功能

## 这是个啥？

selectInput 是将input框变成即可输入亦可选择的select下拉组件，主要使用场景是用户可以输入关键词，匹配系统存在的联想词形成select下拉框以供用户选择，当然用户也可以使用自行输入的信息，支持模糊匹配，数据源可以本地赋值，也可以异步url请求加载，或者干脆点直接实时输入远程请求联想呈现

![案例截图](https://images.gitee.com/uploads/images/2021/0617/155329_4b0c610b_1476573.png "案例截图.png")
*****
## 怎么使用呢?

1. 下载源代码, 通常情况下, 你可以在 [Releases](https://gitee.com/JerryZst/select-input/releases) 这里找到所有已经发布的版本.
2. 将下载好的文件, 通常是压缩包, 解压到你项目的扩展目录里去, 譬如: `libs/modules`
3. 确认项目的 `layui.config` 和 `layui.base` 配置是否正确, 可参考 [示例文件](./index.html)
4. 使用 `layui.use` 来引入扩展! 可参考 [示例文件](./index.html)


## 代码片段

> 仅供参考, 请勿复制粘贴照搬照抄，否则导致的报错或者无法运行自行负责


### 配置扩展

```javascript
layui
    .config({
        base: "./modules/",
    })
    .extend({
        selectInput: "selectInput/selectInput",
    });
```

### 引入扩展并使用

#### HTML 部分

```html
 <div class="layui-form-item">
        <div class="layui-input-block" id="test1"></div>
 </div>
```

#### Javascript 部分

```javascript
layui.use(["selectInput"], function () {
    var selectInput = layui.selectInput;
    
    // 全量参数版本
   var ins = selectInput.render({
            // 容器id，必传参数
            elem: '#test1',
            name: 'test', // 渲染的input的name值
            layFilter: 'test', //同layui form参数lay-filter
            layVerify: 'required', //同layui form参数lay-verify
            layVerType: 'tips', // 同layui form参数lay-verType
            layReqText: '请填写文本', //同layui form参数lay-ReqText
            initValue: 'iPhone12 Pro Max', // 渲染初始化默认值
            hasSelectIcon: false,
            placeholder: '请输入名称', // 渲染的inputplaceholder值
            // 联想select的初始化本地值，数组格式，里面的对象属性必须为value，name，value是实际选中值，name是展示值，两者可以一样
            data: [
                {value: 1111, name: 1111},
                {value: 2333, name: 2222},
                {value: 2333, name: 2333},
                {value: 2333, name: 2333},
            ],
            url: "https://www.layui.com/", // 异步加载的url，异步加载联想词的数组值，设置url，data参数赋的值将会无效，url和data参数两者不要同时设置
            remoteSearch: false, // 是否启用远程搜索 默认是false，和远程搜索回调保存同步
            parseData: function (data) {  // 此参数仅在异步加载data数据下或者远程搜索模式下有效，解析回调，如果你的异步返回的data不是上述的data格式，请在此回调格式成对应的数据格式，回调参数是异步加载的数据

            },
            error: function (error) { // 异步加载出错的回调 回调参数是错误msg

            },
            done: function (data) { // 异步加载成功后的回调 回调参数加载返回数据

            },
            remoteMethod: function (value, cb) { // 远程搜索的回调函数
                // value 是input实施输入的value值
                // cb是回调处理函数，请在执行ajax搜索请求成功之后执行此回调函数

                //案例
                //这里如果val为空, 则不触发搜索
                if (!value) {
                    return cb([]);
                }
                //这里的$.ajax去请求后台拿回数据
                $.ajax({
                    method: 'get',
                    url: 'https://your.domain/search',
                    data: {
                        keyword: val,
                    },
                    success: function (data) {
                        // 此处最好直接返回取你返回的 data属性，如：data.data，这样不限制你后端返回的数据对象格式，如果整体传参，必须满足：{"code":0,"msg":"success","data":[]}
                        return cb(data)
                    }
                })
            }
        }
    });

    // 如果使用本地数据加载，不要使用设置远程 url参数 完整的本地赋值数据案例

    var ins = selectInput.render({
          elem: '#test1',
          data: [
           {value: 1111, name: 1111},
           {value: 2333, name: 2222},
           {value: 2333, name: 2333},
           {value: 2333, name: 2333},
          ],
          placeholder: '请输入名称',
          name: 'list_common',
          remoteSearch: false
    });
    
     // 监听input 实时输入事件
    ins.on('itemInput(test1)', function (obj) {
        console.log(obj);
    });

    // 监听select 选择事件
    ins.on('itemSelect(test1)', function (obj) {
        console.log(obj);
    });
    
     // 动态添加select选项，第二个默认为false，即为覆盖data重载，true为push追加
    ins.addSelect([
        {value: 1111, name: 1111},
        {value: 2333, name: 2222},
        {value: 2333, name: 2333},
        {value: 2333, name: 2333},
        {value: 6666, name: 9999},
        {value: 8888, name: 101010},
    ],false);

      // 获取选中的value值
    var selectValue = ins.getValue();
    
    // 清空输入框的value值
    ins.emptyValue();
    
});
 ```

## 文档

你可以点击 [在线文档](./README.md) 开始阅读文档, 如果你已经拉取了完整的项目, 可以在本地直接打开 `README.md` 来阅读离线文档！具体参数使用请移步到 [使用参数文档](./doc.md)

## 小的Tips

>PS：有问题的话可以在评论区回复，会尽量完善这个组件的

加url的那种异步加载适用的场景是：你的data数据需要在页面渲染的时候异步加载请求后台拿到数据，然后后续搜索是基于这个数据进行前端搜索的,所以url异步加载和data赋值模式两者互斥，而远程实时搜索这种和url异步加载/data赋值模式两者是互斥的，本质上url异步加载和data直接赋值都是前端基于已有的数据进行搜索，而远程实时搜索，前端不参与搜索匹配，而会调用你完成的remoteMethod远程搜索函数去后台搜索完后返回一个data列表渲染；parseData解析回调仅在url异步加载和远程实时搜索的时候有效，如果设置了，必须返回数据，否则页面无法进行搜索显示功能**

## 支持作者

最简单的粗暴的方式就是直接使用你们的**钞能力**, 当然这是您自愿的! **点击可直接查看大图**

### 赞赏通道

<img src="./images/zhifubao.jpg" height="200px" title="支付宝">
<img src="./images/wxpay.jpg" height="200px" title="微信">
<img src="./images/qqpay.jpg" height="200px" title="QQ">

当然, 如果客观条件不允许或者主观上不愿意, 也没啥关系嘛! 码农的钱挣得都是辛苦钱啊. 所以除了使用 **钞能力**, 你还可以通过以下方式支持作者!

1. 给项目点个 Star, 让更多的小伙伴知道这个扩展!
2. 积极测试, 反馈 BUG, 如果发现代码中有不合理的地方积极反馈!
3. 加入粉丝群, 看看有多少志同道合的小伙伴! <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=5qRRxB1XRBphoKwyNGe7vZ_b_WPlgQPb&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="selectInput交流" title="selectInput交流"></a>

![QQ群二维码](https://images.gitee.com/uploads/images/2021/0617/162328_bedc2fe6_1476573.png "QQ群二维码.png")
