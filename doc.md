#### 组件实例参数


|字段  | 类型 | 描述 | 是否必须|
|---|---|---|---|
elem | string | 容器id | 是
name | string | 渲染的input的name值  | 是
layFilter | string | 同layui form参数lay-filter | 否
layVerify | string | 同layui form参数lay-verify  | 否
layVerType | string | 同layui form参数lay-verType | 否
layReqText | string | 同layui form参数lay-ReqText | 否
hasSelectIcon | boolean | 是否点击隐藏模式（如果传true的话，显示select下拉显示图标，点击显示隐藏下拉列表，<br/>鼠标移动隐藏下拉列表会失效）  | 否(默认false)
uniqueId | string | 实例唯一ID  | 否
invisibleMode | boolean | 实例方法getValue返回数据模式<br>(调用getValue方法获取值，此处可能有用户自行输入的值，<br>此时拿不到传入的对象里面的value值，只能返回实时input的value值，<br>故新增实例参数invisibleMode，默认为false，<br>此项参数如果为true在调用实例方法getValue方法时返回的是对象，<br>如：{"value":"21111","isSelect":false}，<br>对象属性value即为实例value值，而isSelect则表示是否是选中的值，而非用户自行输入的值) | 否 (默认false)
hasInitShow | boolean | 组件渲染完是否立即展开下拉选项 | 否 (默认false)
hasCut| boolean |  是否启用input剪贴事件 | 否 (默认true，false即为禁用)
ignoreCase | boolean |  前端联想匹配是否忽略大小写 | 否 (默认false，false即为强匹配，true即为忽略大小写匹配)
placeholder |  string |  渲染的inputplaceholder值 | 否
data | array | 下拉列表数据的初始化本地值<br>(如果是异步加载的模式请不要传入data参数) | 否
url | string | 下拉列表数据异步加载地址<br> 异步加载返回的数据格式必须为:<br> {"code":0,"msg":"success","data":[{"name":"test","value":1}]} | 否
parseData | function | 此参数仅在异步加载data数据下或者远程搜索模式下有效，解析回调<br> 此回调函数一定要return数据，否则无法渲染 | 否
remoteSearch | boolean | 是否启用远程搜索 默认是false，和远程搜索回调保存同步 | 否 (默认false)
remoteMethod | function | 远程搜索的回调函数 | 否
remoteEvent  |  string  | 远程搜索响应的事件 （默认为input实时输入事件，目前还支持键盘回车事件，传：keydown）| 否
error | function | 异步加载出错的回调 回调参数是错误msg | 否
done | function | 异步加载成功后的回调 回调参数加载返回数据 | 否

#### 组件实例方法

```html
    
    var ins2 = selectInput.render({
        elem: '#test1',
        data: [
            {value: 1111, name: 1111},
            {value: 2333, name: 2222},
            {value: 2333, name: 2333},
            {value: 2333, name: 2333},
        ],
        placeholder: '请输入名称',
        name: 'list_common',
        remoteSearch: false
    }); 
    
    // 监听input 实时输入事件
    ins.on('itemInput(test1)', function (obj) {
        console.log(obj);
    });

    // 监听select 选择事件
    ins.on('itemSelect(test1)', function (obj) {
        console.log(obj);
    });

     // 获取选中的value值
    var selectValue = ins.getValue();
    
    // 清空输入框的value值
    ins.emptyValue();

    // 设置属性值
    ins.setValue(1, true);

   // 动态添加select选项，第二个默认为false，即为覆盖data重载，true为push追加
    ins.addSelect([
        {value: 1111, name: 1111},
        {value: 2333, name: 2222},
        {value: 2333, name: 2333},
        {value: 2333, name: 2333},
        {value: 6666, name: 9999},
        {value: 8888, name: 101010},
    ],false);
    
    // 展开所有下拉选项
    ins.showElem();

```

### 2021-07-28更新
> 新增实例方法：setValue，用于动态设置input值，setValue方法接受两个参数，第一个参数是属性值，会在既有的数据list里面匹配value值，得到对应的name值并设置为input显示值，第二个参数为bool类型，默认为false，如果传递true，即表示如果没有匹配到对应的value值时是不会更改input显示值，否则没有找到的话会直接将传入值设置为input显示值

### 2021-06-17更新
> 实例化新增参数：ignoreCase，前端联想匹配是否忽略大小写 (默认false，false即为强匹配，true即为忽略大小写匹配，远程后端搜索联想请自行实现此逻辑)

### 2021-04-13更新

> 新增实例参数hasInitShow，默认为false，如果为true会在渲染完列表之后，自动展开所有选项显示，新增实例方法showElem()，调用之后会展开所有选项显示，如果选项已经显示，则会收起隐藏

### 2021-03-16更新

> 调整了之前的显示逻辑，选中点击下拉列表，input框始终显示用name值，而value则在getValue方法返回中，但是此处可能有用户自行输入的值，此时拿不到传入的对象里面的value值，只能返回实时input的value值，故新增实例参数invisibleMode，默认为false，此项参数如果为true在调用实例方法getValue方法时返回的是对象，如：{"value":"21111","isSelect":false}，对象属性value即为实例value值，而isSelect则表示是否是选中的值，而非用户自行输入的值

### 2021-01-12更新
> 新增实例方法：addSelect，用于动态添加select选项，addSelect方法接受两个参数，第一个参数是数组格式，和实例化参数data格式一致，第二个参数是添加的选项是重载还是push到已有data数组后面

### 2021-01-11更新
> 实例化新增参数：uniqueId，用于同个页面存在多个实例id不唯一的情况，如果不传的话，自动在时间戳id后面补上随机数

### 2021-01-10更新
> 实例化新增参数：hasSelectIcon，此参数非必传，如果传true的话，input框右边会显示select下拉显示图标，可以点击显示隐藏下拉列表，同时移动隐藏下拉列表会失效

### 2020-12-22更新
> 1: 新增实例方法：getValue， 获取获取选中的value值

> 2: 新增实例方法：emptyValue，清空输入框的value值



### 2020-12-08更新

> 1: 允许value和name值不同，显示用name，取值用value

> 2: 新增参数initValue，设置input默认初始值

> 3: 修复了一些已知的bug问题